<?php

use yii\db\Migration;

/**
 * Class m210218_182716_newTableApple
 */
class m210218_182716_newTableApple extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('apple', [

            'id' => $this->primaryKey(),
            'size' => $this->decimal(2)->defaultValue(1.00),
            'color' => $this->string(),
            'date_creation' => $this->integer(),
            'date_fall' => $this->integer(),
            'is_on_tree' => $this->smallInteger()->defaultValue(1),
            'is_rotten' => $this->smallInteger()->defaultValue(0),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('apple');
    }
}
