<?php


namespace yii\console\controllers;

use common\config\ProjectConstants;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception;

/**
 * Class CheckApplesController contains commands for cron.
 * Checks apples state and is sorting them.
 * @package yii\console\controllers
 */
class CheckApplesController extends Controller
{
    /**
     * Finds all records in `apple` table` that were expired by date_fall
     * @return int
     */
    public function actionCheckApples()
    {
        $intervalHours = ProjectConstants::HOURS_FOR_ROTTEN;

        $query = <<<EOL
            update apple
    set 
        is_rotten = 1,
        color = 'brown',
        status = 0
    where (status = 1 and is_on_tree = 0 and date_fall <= UNIX_TIMESTAMP() - 3600*{$intervalHours});
EOL;

        try {
            $rowsAffected = \Yii::$app->db->createCommand($query)->execute();
            \Yii::info('Было отсортировано яблок: ' . $rowsAffected, 'applesInspection');

        } catch (Exception $exception) {
            \Yii::info($exception->getMessage(), 'applesInspectionError');
        }

        return ExitCode::OK;
    }
}