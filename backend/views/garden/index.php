<?php

/**
 * @var $model \common\models\tables\Apple
 * @var $dataProvider
 */

use backend\assets\GardenAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\ListView;
use common\config\ProjectConstants;
use common\widgets\Alert;

GardenAsset::register($this);

?>

<?php

echo Alert::widget();

$min = ProjectConstants::APPLE_MIN_NUMBER;
$max = ProjectConstants::APPLE_MAX_NUMBER;

$placeholder = <<< EOL
Введите текст от {$min} до {$max}
EOL;

$form = ActiveForm::begin(); ?>

<?= $form->field($model, 'number')->label('Количество яблок')->input('integer', ['placeholder' => $placeholder]); ?>

<div class="form-group">
    <?= Html::submitButton('Получить', ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Удалить все', '/admin/garden/delete-all', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end();

?>


<?php

$template = <<< EOL
<div id="content">
    <div class="appleSum">{summary}</div>
    <div class="applePager">{pager}</div>
    <div class="appleItems">{items}</div>
</div>
EOL;

?>

<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_appleListView',
    'layout' => $template,
    'id' => 'applesCards'
]);

?>



