<?php

use app\models\Garden;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var $model \common\models\tables\Apple
 */

?>
<div class="card" style="width: 18rem;">
    <h5 class="card-title"><?php echo 'Яблоко №' . $model->id ?></h5>
    <?php

    Pjax::begin([
        'id' => $model->id,
        'enablePushState' => false,
        'timeout' => 100000,
    ]);


    $form = ActiveForm::begin([
        'action' => Url::to(['garden/proceed-apple', 'id' => $model->id]),
        'options' => [
            'data-pjax' => true,
            'class' => 'form-oneapple'
        ],
        'method' => 'post',
        'id' => 'apples' . $model->id,
    ]);

    if ($model->is_on_tree) {
        echo Html::submitButton('Уронить на землю', ['class' => 'btn btn-primary', 'name' => 'button', 'value' => 'fall']);
    } else {

        echo $form
            ->field($model, 'size')
            ->label('Съесть?')
            ->dropDownList([
                25 => 'Откусить четверть яблока',
                50 => 'Откусить половину',
                75 => 'Съесть три четверти',
                100 => 'Съесть всё',
            ],
                ['options' => [
                    25 => ['Selected' => true]
                ]
                ]
            );

        echo Html::submitButton('Откусить', ['class' => 'btn btn-primary', 'name' => 'button', 'value' => 'eat']);

    }

    ActiveForm::end();

    ?>

    <div class="card-body">
        <p class="card-text"><?php echo Garden::getText($model); ?></p>
    </div>

    <?php

    Pjax::end();

    ?>


    <!-- <img class="card-img-top" src="..." alt="Card image cap">-->
    <!--<div class="card-body">
        <h5 class="card-title"><?php /*echo $model->color */ ?></h5>
        <p class="card-text"><?php /*echo Garden::getText($model); */ ?></p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>-->
</div>
