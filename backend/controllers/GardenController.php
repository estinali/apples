<?php

namespace backend\controllers;

use app\models\Garden;
use common\config\ProjectConstants;
use phpDocumentor\Reflection\Types\Integer;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\Controller;
use common\models\tables\Apple;

/**
 * Class GardenController manages all apples' operations.
 * @package backend\controllers
 */
class GardenController extends Controller
{

    /**
     * Displays all aplles that exists in db table apples with status 1,
     * allows generate new records.
     * @return string
     * @throws Exception
     */
    public function actionIndex()
    {
        $model = new DynamicModel(['number']);
        $model
            ->addRule('number', 'integer', [
                'min' => ProjectConstants::APPLE_MIN_NUMBER,
                'max' => ProjectConstants::APPLE_MAX_NUMBER
            ])
            ->addRule('number', 'default', ['value' => 0]);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $number = (int)\Yii::$app->request->post()['DynamicModel']['number'];

            if ($number) {
                (new Garden())->generateApples($number);
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => Apple::find()->where(['status' => ProjectConstants::STATUS_ACTIVE])->orderBy(['created_at' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 100,
            ]
        ]);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,

        ]);

    }

    /**
     * Sets inactive status (0) to all rows in table `apple`.
     */
    public function actionDeleteAll()
    {
        //Apple::deleteAll(); use it if you really need delete all the records from db

        $message = 'Все яблоки удалены';
        $type = 'success';

        $command = \Yii::$app->db->createCommand('UPDATE apple SET status = 0 WHERE status = 1');
        try {
            $command->execute();
        } catch (\Exception $exception) {
            $message = 'Удаление не удалось. Причина: <br/>' . $exception->getMessage();
            $type = 'error';
        }

        \Yii::$app->session->setFlash($type, $message);
        $this->redirect('/admin/garden');
    }


    /**
     * Proceeds different types of form submit from /garden/index view.
     * @param string $id
     */
    public function actionProceedApple(string $id)
    {
        $apple = Apple::findOne($id);
        $typeButton = \Yii::$app->request->post()['button'];

        if ($typeButton === 'fall') {
            $this->fall($apple);
        }

        if ($typeButton === 'eat') {
            $this->eat($apple);
        }

        if ($apple->status) {
            echo $this->renderPartial('_appleListView', [
                'model' => $apple,
            ]);
        } else {
            \Yii::$app->session->addFlash('error', 'Яблоко ' . $apple->id . ' съедено');
            $this->redirect('/admin/garden');
        }

    }

    /**
     * Defines that apple was fallen from a tree.
     * @param Apple $apple
     */
    public function fall(Apple $apple)
    {
        $apple->fallToGround();
        $apple->save();
    }

    /**
     * Checks if an apple is eatable and corrects size according to new data.
     * @param $apple
     * @param int $percentage
     */
    public function eat($apple, $percentage = 0)
    {
        $percentage = (int)\Yii::$app->request->post()['Apple']['size'];

        try {
            $apple->eat($percentage);
        } catch (\Exception $exception) {
            \Yii::$app->session->addFlash('error', $exception->getMessage());
            $this->redirect('/admin/garden');
        }
    }

}