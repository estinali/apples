<?php

namespace app\models;

use common\config\ProjectConstants;
use common\models\tables\Apple;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class Garden
 * Serves for manipulation with different data connected with table `apple`.
 * @package app\models
 */
class Garden extends Model
{
    /**
     * Generates new records in table `apple` due to given number.
     * @param $number
     * @throws Exception
     */
    public function generateApples($number)
    {
        while ($number) {

            $apple = new Apple();
            if (!$apple->save(false)) {
                throw new Exception('Яблоко пропало и не записано в базу данных!');
            }
            $apples[] = $apple;
            $number--;
        }
    }

    /**
     * Composes text for a model description on /garden/index.
     * @param $model
     * @return string
     */
    public static function getText($model)
    {
        $size = '';

        switch ($model->size) {
            case 1:
                $size = ' целое яблоко';
                break;
            case 0.75:
                $size = ' почти целое яблоко';
                break;
            case 0.5:
                $size = ' половина яблока';
                break;
            case 0.25:
                $size = ' маленький кусочек яблока';
                break;
            case 0:
                $size = ' уже и не яблоко';
                break;
        }

        $color = '';

        switch ($model->color) {
            case 'green':
                $color = 'зеленый';
                break;
            case 'yellow':
                $color = 'желтый';
                break;
            case 'red':
                $color = 'красный';
                break;
            default:
                $color = $model->color;
        }

        return <<<EOL
Это {$size}. <br />
Цвет: {$color}. <br />
EOL;
    }

}