<?php

namespace common\models\tables;

use common\config\ProjectConstants;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property float|null $size
 * @property string|null $color
 * @property int|null $date_creation
 * @property int|null $date_fall
 * @property int|null $is_on_tree
 * @property int|null $is_rotten
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Apple extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */

    public function init()
    {
        parent::init();

        if (is_null($this->color)) {
            $this->setRandomColor();
        }

        $this->setRandomDate();
        $this->setRandomColor();

    }

    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_on_tree', 'is_rotten', 'date_creation', 'date_fall', 'created_at', 'updated_at'], 'integer'],
            [['color'], 'string', 'max' => 255],
            [['size'], 'double'],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'size' => 'Size',
            'color' => 'Color',
            'date_creation' => 'Date Creation',
            'date_fall' => 'Date Fall',
            'is_on_tree' => 'Is On Tree',
            'is_rotten' => 'Is Rotten',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fallToGround()
    {
        $this->is_on_tree = 0;
        $this->date_fall = time();

    }

    public function eat($percent)
    {
        if ($this->is_on_tree) {
            throw new Exception('Сначала яблоко нужно снять с дерева');
        }

        if ($this->is_rotten) {
            throw new Exception('Это яблоко испорчено, не ешь его');
        }

        $newSize = $this->size - $percent / 100;

        if ($newSize <= 0) {
            $newSize = 0;
            $this->status = 0;
        }

        $this->size = $newSize;
        $this->save();

    }

    public function delete()
    {
        $this->status = 0;
        $this->save();
    }


    /**
     * Sets random color to apple.
     * @return string
     */
    public function setRandomColor()
    {
        $colors = (new ProjectConstants())->getColors();
        $this->color = $colors['fresh'][array_rand($colors['fresh'])];
    }

    public function setRandomDate()
    {
        $today = time();
        $this->date_creation = mt_rand($today-20000, $today);
    }

    public function checkIsRotten()
    {
        $timeOnGround = time() - $this->date_fall;

        if ($timeOnGround >= ProjectConstants::HOURS_FOR_ROTTEN_IN_SEC) {
            $this->is_rotten = 1;
            $this->color = (new ProjectConstants())->getColors()['rotten'];
        }

    }

}
