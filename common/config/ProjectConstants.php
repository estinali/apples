<?php

namespace common\config;

class ProjectConstants
{
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 0;

    const APPLE_MIN_NUMBER = 1;
    const APPLE_MAX_NUMBER = 100;

    const HOURS_FOR_ROTTEN_IN_SEC = 5 * 60 * 60;
    const HOURS_FOR_ROTTEN = 5;

    public $colors = [
        'fresh' => ['red', 'green', 'yellow'],
        'rotten' => 'brown',
    ];

    public function getColors()
    {
        return $this->colors;
    }

}